$(document).ready(function () {

    // ------------------------------------------------------- //
    // Custom Scrollbar
    // ------------------------------------------------------ //

    if ($(window).outerWidth() > 992) {
        $("nav.side-navbar").mCustomScrollbar({
            scrollInertia: 200
        });
    }

    // ------------------------------------------------------- //
    // Side Navbar Functionality
    // ------------------------------------------------------ //
    $('#toggle-btn').on('click', function (e) {

        e.preventDefault();

        if ($(window).outerWidth() > 1194) {
            $('nav.side-navbar').toggleClass('shrink');
            $('.page').toggleClass('active');
        } else {
            $('nav.side-navbar').toggleClass('show-sm');
            $('.page').toggleClass('active-sm');
        }
    });
});
$(document).ready(function() {
    $('.btnNext').click(function() {
        $('#campaignTab.nav-tabs .active').parent().next('li').find('a').trigger('click');
    });

    $('.btnPrevious').click(function() {
        $('#campaignTab.nav-tabs .active').parent().prev('li').find('a').trigger('click');
    });
});

$(document).ready(function() {
    $('#editbutton').click(function() {
        $('.test-form').removeClass('form-info');
        $('.testsavebtn').show();
        $('#editbutton').hide();
    });


});


